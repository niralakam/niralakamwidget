import 'dart:convert';
import 'package:niralakam_widget/model/about_widget_model.dart';
import 'package:niralakam_widget/model/card_horizontal_first_widget_model.dart';
import 'package:niralakam_widget/model/card_horizontal_first_with_sub_widget_model.dart';
import 'package:niralakam_widget/model/card_horizontal_second_widget_model.dart';
import 'package:niralakam_widget/model/card_image_model.dart';
import 'package:niralakam_widget/model/card_vertical_content_widget_model.dart';
import 'package:niralakam_widget/model/dropdown_model.dart';
import 'package:niralakam_widget/model/dropdown_weapons_model.dart';
import 'package:niralakam_widget/model/map_details_widget_model.dart';

class NiralakamConvertor {
  static List<CardImageModel> cardWithImageOnlyFromMap(String str) =>
      List<CardImageModel>.from(
          json.decode(str).map((x) => CardImageModel.fromMap(x)));

  static List<CardHorizontalFirstWidgetModel>
      cardHorizontalFirstWidgetModelFromMap(String str) =>
          List<CardHorizontalFirstWidgetModel>.from(json
              .decode(str)
              .map((x) => CardHorizontalFirstWidgetModel.fromMap(x)));

  static List<CardHorizontalFirstWithSubWidgetModel>
      cardHorizontalFirstWithSubWidgetModelFromMap(String str) =>
          List<CardHorizontalFirstWithSubWidgetModel>.from(json
              .decode(str)
              .map((x) => CardHorizontalFirstWithSubWidgetModel.fromMap(x)));

  static List<AboutWidgetModel> aboutWidgetModelFromMap(String str) =>
      List<AboutWidgetModel>.from(
          json.decode(str).map((x) => AboutWidgetModel.fromMap(x)));

  static List<CardHorizontalSecondWidgetModel>
      cardHorizontalSecondWidgetModelFromMap(String str) =>
          List<CardHorizontalSecondWidgetModel>.from(json
              .decode(str)
              .map((x) => CardHorizontalSecondWidgetModel.fromMap(x)));

  static String cardHorizontalSecondWidgetModelToMap(
          List<CardHorizontalSecondWidgetModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

  static List<CardVerticalContentWidgetModel>
      cardVerticalContentWidgetModelFromMap(String str) =>
          List<CardVerticalContentWidgetModel>.from(json
              .decode(str)
              .map((x) => CardVerticalContentWidgetModel.fromMap(x)));

  static List<DropdownModel> dropdownModelFromMap(String str) =>
      List<DropdownModel>.from(
          json.decode(str).map((x) => DropdownModel.fromMap(x)));

  static List<DropdownWeaponsModel> dropdownWeaponsModelFromMap(String str) =>
      List<DropdownWeaponsModel>.from(
          json.decode(str).map((x) => DropdownWeaponsModel.fromMap(x)));

  static List<MapDetailsWidgetModel> mapDetailsWidgetModelFromMap(String str) =>
      List<MapDetailsWidgetModel>.from(
          json.decode(str).map((x) => MapDetailsWidgetModel.fromMap(x)));
}
