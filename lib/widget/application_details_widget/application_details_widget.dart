import 'package:flutter/material.dart';
import 'package:niralakam_widget/theme/niralakam-share-theme.dart';

class ApplicationDetailsWidget extends StatelessWidget {
  final AnimationController animationController;
  final Animation animation;
  final String description;
  const ApplicationDetailsWidget({
    Key key,
    this.animationController,
    this.animation,
    this.description,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: new Transform(
            transform: NiralakamSharedTheme.transform(animation),
            child: Padding(
              padding: NiralakamSharedTheme.edgeInsetsPaddingDoubleExtraLargeLR,
              child: Container(
                decoration: NiralakamSharedTheme.decoration,
                child: Padding(
                  padding: NiralakamSharedTheme
                      .edgeInsetsPaddingDoubleExtraLargeLTRB,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              description,
                              style: NiralakamSharedTheme.subContentHeader,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
