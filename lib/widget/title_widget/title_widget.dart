import 'package:flutter/material.dart';
import 'package:niralakam_widget/theme/niralakam-share-theme.dart';

class TitleWidget extends StatelessWidget {
  final String titleTxt;
  final String subTxt;
  final AnimationController animationController;
  final Animation animation;
  final MaterialColor color;
  const TitleWidget(
      {this.titleTxt: "",
      this.subTxt: "",
      this.animationController,
      this.animation,
      this.color})
      : super();

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: new Transform(
            transform: NiralakamSharedTheme.transform(animation),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: NiralakamSharedTheme.paddingDoubleExtraLarge,
                  right: NiralakamSharedTheme.paddingDoubleExtraLarge,
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        titleTxt,
                        textAlign: TextAlign.left,
                        style: MediaQuery.of(context).size.width >= 375
                            ? NiralakamSharedTheme
                                .textStyleLargeWithPrimaryColor
                            : NiralakamSharedTheme
                                .textStyleMediumWithPrimaryColor,
                      ),
                    ),
                    titleViewSubConent(subTxt)
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget titleViewSubConent(input) {
    if (input != null && input != '') {
      return InkWell(
        highlightColor: Colors.transparent,
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
        onTap: () {},
        child: Padding(
          padding: const EdgeInsets.only(left: 8),
          child: Row(
            children: <Widget>[
              Text(
                subTxt,
                textAlign: TextAlign.left,
                style: NiralakamSharedTheme.textStyleSmallWithOpacity,
              ),
              SizedBox(
                height: 38,
                width: 26,
                child: Icon(
                  Icons.arrow_forward,
                  color: NiralakamSharedTheme.darkText,
                  size: 18,
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}
