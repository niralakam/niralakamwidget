class DropdownWeaponsModel {
  DropdownWeaponsModel({
    this.key,
    this.value,
  });

  String key;
  String value;

  DropdownWeaponsModel copyWith({
    String key,
    String value,
  }) =>
      DropdownWeaponsModel(
        key: key ?? this.key,
        value: value ?? this.value,
      );

  factory DropdownWeaponsModel.fromMap(Map<String, dynamic> json) =>
      DropdownWeaponsModel(
        key: json["key"] == null ? "" : json["key"],
        value: json["value"] == null ? "" : json["value"],
      );
}
