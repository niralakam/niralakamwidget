class DropdownModel {
  DropdownModel({
    this.key,
    this.name,
  });

  String key;
  String name;

  DropdownModel copyWith({
    String key,
    String name,
  }) =>
      DropdownModel(
        key: key ?? this.key,
        name: name ?? this.name,
      );

  factory DropdownModel.fromMap(Map<String, dynamic> json) => DropdownModel(
        key: json["key"] == null ? "" : json["key"],
        name: json["name"] == null ? "" : json["name"],
      );
}
