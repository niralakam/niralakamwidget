class CardImageModel {
  CardImageModel({
    this.key,
    this.src,
  });

  String key;
  String src;

  CardImageModel copyWith({
    String key,
    String src,
  }) =>
      CardImageModel(
        key: key ?? this.key,
        src: src ?? this.src,
      );

  factory CardImageModel.fromMap(Map<String, dynamic> json) => CardImageModel(
        key: json["key"] == null ? "" : json["key"],
        src: json["src"] == null ? "" : json["src"],
      );
}
